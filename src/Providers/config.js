export default () => ({
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": process.env.WORDPRESS_URL,
    "Access-Control-Allow-Headers": "*",
    Authorization: `Bearer ${localStorage.getItem("tokeninstein")}`
  }
});
