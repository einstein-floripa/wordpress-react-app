import config from "./config";

const BLOB = async url => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_BACKEND_URL}${url}`,
      config()
    );
    return await response.blob();
  } catch (err) {
    throw new Error(`Erro na chamada BLOB: ${url}`);
  }
};

const GET = async url => {
  try {
    const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}${url}`, {
      ...config(),
      referrer: "no-referrer"
    });
    console.log("resp", response);
    if (response.status === 403) {
      localStorage.removeItem("tokeninstein");
      window.location = "/chamada-v1/"; // alterar depois para history.push
      return {};
    } else {
      const json = response.status !== 204 ? await response.json() : {};
      return json;
    }
  } catch (err) {
    console.log("err: ", err);
    throw new Error(`Erro na chamada GET: ${url}`);
  }
};

const POST = async (url, body) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}${url}`, {
      ...config(),
      method: "POST",
      body: JSON.stringify(body),
      referrer: "no-referrer"
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada POST: ${url}`);
  }
};

const PUT = async (url, body) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}${url}`, {
      ...config(),
      method: "PUT",
      body: JSON.stringify(body)
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada PUT: ${url}`);
  }
};

const DELETE = async (url, body) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}${url}`, {
      ...config(),
      method: "DELETE",
      body: JSON.stringify(body)
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada DELETE: ${url}`);
  }
};

const UPLOAD = async (url, body) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}${url}`, {
      headers: {
        //tokenHash: config().headers.tokenHash,
      },
      method: "POST",
      body
    });
    try {
      return await response.json();
    } catch (err) {
      return {};
    }
  } catch (err) {
    throw new Error(`Erro na chamada UPLOAD: ${url}`);
  }
};

const GETWP = async url => {
  try {
    const response = await fetch(
      `${process.env.REACT_APP_WORDPRESS_URL}${url}`
    );
    const json = await response.json();
    return json;
  } catch (err) {
    console.log("err: ", err);
    throw new Error(`Erro na chamada GETWP: ${url}`);
  }
};

export default {
  GET,
  POST,
  PUT,
  DELETE,
  UPLOAD,
  BLOB,
  GETWP
};
