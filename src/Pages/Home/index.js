import React from "react";
import Blog from "../Blog";
import { Layout, Icon } from "antd";
import MenuSuperior from "../../Components/MenuSuperior";

const { Header, Footer, Content } = Layout;

export default class Home extends React.Component {
  render() {
    return (
      <Layout>
        <Header
          style={{
            backgroundColor: "#ffffff"
          }}
        >
          <MenuSuperior />
        </Header>
        <Content
          style={{
            backgroundColor: "#1d74a7",
            height: "calc(100vh - 133px)"
          }}
        >
          <Blog />
        </Content>
        <Footer
          style={{
            backgroundColor: "#ffffff"
          }}
        >
          Feito com <Icon type="heart" theme="twoTone" twoToneColor="#eb2f96" />{" "}
          pela Matrix
        </Footer>
      </Layout>
    );
  }
}
