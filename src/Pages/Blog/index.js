import React, { Component } from "react";
import { Input, Row, Col } from "antd";
import ContentCards from "./ContentCards";
import styled from "styled-components";

const Content = styled.div`
  padding: 16px;

  .ant-input-search > .ant-input {
    background-color: #1d74a7;
    border: none;
    border-radius: 0;
    border-bottom: 1px solid white;
    color: white;
  }

  .ant-input-search > .ant-input:hover {
    border: none;
    border-radius: 0;
    border-bottom: 1px solid white;
  }

  .ant-input-search > .ant-input:focus {
    box-shadow: none;
  }

  .ant-input-suffix > .anticon {
    color: white;
    font-size: 24px;
  }
`;
class Blog extends Component {
  state = { search: "" };

  render() {
    const { search } = this.state;

    return (
      <Content>
        <Row>
          <Col span={5} style={{ padding: "0 16px" }}>
            <Input.Search
              size="large"
              placeholder="Pesquise uma postagem"
              onSearch={search => this.setState({ search })}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: 10 }}>
          <Col>
            <ContentCards search={search} />
          </Col>
        </Row>
      </Content>
    );
  }
}

export default Blog;
