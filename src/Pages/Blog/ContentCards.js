import React, { Component } from "react";
import { Spin, Row } from "antd";
import BaseProvider from "../../Providers/BaseProvider";
import NormalCard from "../../Components/NormalCard";
import FirstCard from "../../Components/FirstCard";
import styled from "styled-components";

const Content = styled.div`
  .ant-card-body {
    padding: 0;
  }
`;

class ContentCards extends Component {
  state = { contents: [] };

  componentDidMount() {
    this.getPosts();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.search !== this.props.search) {
      this.getPosts();
    }
  }

  getPosts() {
    const { search } = this.props;
    this.setState({ loading: true }, () => {
      BaseProvider.GETWP(
        `/posts${search !== "" ? `?search=${search}` : ""}`
      ).then(posts => {
        Promise.all(
          posts.map(async p => {
            const i = await BaseProvider.GETWP(`/media/${p.featured_media}`);
            return { post: p, image: i };
          })
        ).then(contents => this.setState({ contents, loading: false }));
      });
    });
  }

  render() {
    const { contents, loading } = this.state;

    if (loading) return <Spin />;

    if (contents.length <= 0) {
      return (
        <div>
          <h1>Nenhum post encontrado</h1>
        </div>
      );
    }

    return (
      <Content>
        <Row gutter={32} style={{ width: "100%", margin: 0 }}>
          {contents.map((content, index) => {
            return index === 0 ? (
              <FirstCard key={content.post.id} content={content} />
            ) : (
              <NormalCard key={content.post.id} content={content} />
            );
          })}
        </Row>
      </Content>
    );
  }
}

export default ContentCards;
