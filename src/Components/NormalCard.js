import React from "react";
import { Col } from "antd";
import ReadNow from "./ReadNow";
import CardBlog from "./CardBlog";

export default ({ content }) => {
  return (
    <Col span={6}>
      <CardBlog
        onClick={() => {
          console.log("click: ", content);
          window.location.href = content.post.link;
        }}
      >
        <div>
          <div style={{ height: 300, width: "100%" }}>
            <div
              style={{
                display: "block",
                textAlign: "center",
                height: "100%",
                width: "100%"
              }}
            >
              <img
                style={{
                  display: "block",
                  height: "100%",
                  width: "auto"
                }}
                src={content.image.source_url}
                alt={content.image.alt_text}
              />
            </div>
            <h1
              className="einst-text"
              style={{
                position: "absolute",
                bottom: "20%",
                left: 0,
                right: 0,
                margin: "0 auto",
                color: "white",
                WebkitTextStrokeWidth: "thin",
                WebkitTextStrokeColor: "black"
              }}
              dangerouslySetInnerHTML={{ __html: content.post.title.rendered }}
            />
          </div>

          <div style={{ display: "flex" }}>
            <ReadNow />
          </div>
        </div>
      </CardBlog>
    </Col>
  );
};
