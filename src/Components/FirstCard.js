import React from "react";
import { Col } from "antd";
import ReadNow from "./ReadNow";
import CardBlog from "./CardBlog";

export default ({ content }) => {
  return (
    <Col span={12}>
      <CardBlog
        onClick={() => {
          console.log("click: ", content);
          window.location.href = content.post.link;
        }}
        hoverable
      >
        <div style={{ display: "flex" }}>
          <div
            style={{
              display: "inline-block",
              alignItems: "center",
              width: 280,
              height: 332
            }}
          >
            <div style={{ width: "100%", height: "100%", overflow: "hidden" }}>
              <img
                src={content.image.source_url}
                alt={content.image.alt_text}
                style={{ height: "100%", width: "auto" }}
              />
            </div>
          </div>

          <div style={{ height: 332 }}>
            <div style={{ height: 300 }}>
              <h1
                className="einst-text"
                style={{ margin: 0, paddingTop: 16 }}
                dangerouslySetInnerHTML={{
                  __html: content.post.title.rendered
                }}
              />
              <p
                className="einst-text"
                style={{ padding: 32 }}
                dangerouslySetInnerHTML={{
                  __html: content.post.excerpt.rendered.replace(
                    "Continue reading &rarr;",
                    ""
                  )
                }}
              />
            </div>

            <div style={{ display: "flex" }}>
              <ReadNow />
            </div>
          </div>
        </div>
      </CardBlog>
    </Col>
  );
};
