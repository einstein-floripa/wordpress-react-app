import React from "react";
import { einstein_floripa_horizontal } from "../assets/images";
import { Menu } from "antd";
import styled from "styled-components";
const { SubMenu } = Menu;

const Item = styled(Menu.Item)`
  .ant-menu-horizontal > .ant-menu-item,
  .ant-menu-horizontal > .ant-menu-submenu {
    color: #9c6fa8;
  }
`;

export default class MenuSuperior extends React.PureComponent {
  state = { current: "/blog" };

  handleSelect = e => {
    console.log(e);
    if (e.key) {
      window.location = `https://einsteinfloripa.com.br${e.key}`;
    }
  };

  render() {
    return (
      <Menu
        onSelect={this.handleSelect}
        selectedKeys={[this.state.current]}
        mode="horizontal"
      >
        <Item key="/">
          <img
            src={einstein_floripa_horizontal}
            alt="Einstein Floripa"
            height="64px"
          />
        </Item>
        <Item key="/home">HOME</Item>
        <SubMenu key="/processos-seletivos" title="PROCESSOS SELETIVOS">
          <Item key="/processos-seletivos/alunos">ALUNOS</Item>
          <Item key="/processos-seletivos/professores-monitores">
            {"PROFESSORES & MONITORES"}
          </Item>
          <Item key="/ps-organizadores">ORGANIZADORES</Item>
        </SubMenu>
        <Item key="/sobre-nos">SOBRE NÓS</Item>
        <SubMenu title="NOSSA EQUIPE">
          <Item key="/professores-monitores">{"PROFESSORES & MONITORES"}</Item>
          <Item key="/organizadores">ORGANIZADORES</Item>
        </SubMenu>
        <Item key="/hall-da-fama">HALL DA FAMA</Item>
        <Item key="/contato">CONTATO</Item>
        <Item key="/blog">BLOG</Item>
        <Item
          key="/amigosdoeinstein"
          style={{
            backgroundImage: "linear-gradient(to left, #1d74a7, #2ca5b2)"
          }}
        >
          APOIE
        </Item>
      </Menu>
    );
  }
}
