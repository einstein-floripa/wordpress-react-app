import styled from "styled-components";
import { Card } from "antd";

export default styled(Card)`
  border-radius: 10px;
  width: 100%;
  overflow: hidden;
  :hover {
    cursor: pointer;
    border-color: rgba(0, 0, 0, 0.09);
    box-shadow: 0 2px 8px rgba(0, 0, 0, 10.09);
  }
`;
