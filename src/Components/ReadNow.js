import React from "react";
import { Button, Icon } from "antd";

export default () => {
  return (
    <Button
      block
      className="einst-btn"
      style={{ border: "none", borderRadius: "0px 0px 4px 4px" }}
    >
      <span style={{ float: "left" }}>Leia agora</span>
      <Icon style={{ float: "right" }} type="plus" />
    </Button>
  );
};
